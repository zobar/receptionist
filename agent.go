package main

import (
	"fmt"
	"github.com/guelfey/go.dbus"
)

type Agent struct {
}

func newAgent() *Agent {
	return &Agent{}
}

func (agent *Agent) Authorize(device dbus.ObjectPath, uuid string) (err error) {
	fmt.Println("Agent Authorize", device, uuid)
	return
}

func (agent *Agent) Cancel() (err error) {
	fmt.Println("Agent Cancel")
	return
}

func (agent *Agent) ConfirmModeChange(mode string) (err error) {
	fmt.Println("Agent ConfirmModeChange", mode)
	return
}

func (agent *Agent) DisplayPasskey(device dbus.ObjectPath, passkey uint32, entered uint8) (err error) {
	fmt.Println("Agent DisplayPasskey", device, passkey, entered)
	return
}

func (agent *Agent) Release() (err error) {
	fmt.Println("Agent Release")
	return
}

func (agent *Agent) RequestConfirmation(device dbus.ObjectPath, passkey uint32) (err error) {
	fmt.Println("Agent RequestConfirmation", device, passkey)
	return
}

func (agent *Agent) RequestPasskey(device dbus.ObjectPath) (passkey uint32, err error) {
	fmt.Println("Agent RequestPasskey", device)
	return
}

func (agent *Agent) RequestPinCode(device dbus.ObjectPath) (pin string, err error) {
	fmt.Println("Agent RequestPinCode", device)
	return
}
