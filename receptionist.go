package main

import (
	"fmt"
	"github.com/zobar/bluez"
	"github.com/zobar/svc/guelfey"
	"log"
)

func adapter(mgr bluez.Manager) {
	adapter, err := mgr.DefaultAdapter()
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("Default adapter: %#v\n", adapter)

	wait := make(chan bool)
	adapter.PropertyChanged(func(name string, value interface{}) {
		fmt.Printf("Adapter property changed: %#v ☞ %#v\n", name, value)
		if name == "Discovering" && value == false {
			close(wait)
		}
	})

	adapterProps(adapter)
	if err := adapter.SetProperty("Discoverable", true); err != nil {
		log.Fatal(err)
	}

	if err := adapter.StartDiscovery(); err != nil {
		log.Fatal(err)
	}

	<-wait

	adapter.StopDiscovery()
	adapter.SetProperty("Discoverable", false)
}

func adapterProps(adapter bluez.Adapter) {
	props, err := adapter.GetProperties()
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("\tProps: %#v\n", props)
	fmt.Printf("\t\tAddress: %#v\n", props.Address())
	fmt.Printf("\t\tClass: %#v\n", props.Class())
	fmt.Printf("\t\tDevices: %#v\n", props.Devices())
	fmt.Printf("\t\tDiscoverable: %#v\n", props.Discoverable())
	fmt.Printf("\t\tDiscoverableTimeout: %#v\n", props.DiscoverableTimeout())
	fmt.Printf("\t\tDiscovering: %#v\n", props.Discovering())
	fmt.Printf("\t\tName: %#v\n", props.Name())
	fmt.Printf("\t\tPairable: %#v\n", props.Pairable())
	fmt.Printf("\t\tPairableTimeout: %#v\n", props.PairableTimeout())
	fmt.Printf("\t\tPowered: %#v\n", props.Powered())
	fmt.Printf("\t\tUUIDs: %#v\n", props.UUIDs())
}

func init() {
	guelfey.Use()
}

func main() {
	daemon := bluez.Daemon()
	fmt.Printf("Daemon: %#v\n", daemon)
	mgr, err := daemon.Manager()
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("Manager: %#v\n", mgr)

	mgrProps(mgr)
	adapter(mgr)
}

func mgrProps(mgr bluez.Manager) {
	props, err := mgr.GetProperties()
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("\tProps: %#v\n", props)
	adapters, err := props.Adapters()
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("\t\tAdapters: %#v\n", adapters)
}
